/*
 * Tetromino
 *
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "queue.h"
#include "tetromino-types.h"

static void totally_random() {
  for (uint32_t i = 0; i < 7; i++) {
    uint8_t type_id = rand() % NUM_TETROMINO_TYPES;

    const TetrominoType *type = &tetromino_types[type_id];
    printf("%c ", type->name);
  }
  printf("\n");
}

static void test_bag() {
  uint8_t *bag = random_bag_new();
  for (uint8_t i = 0; i < 7; i++) {
    const TetrominoType *type = &tetromino_types[bag[i]];
    printf("%c ", type->name);
  }
  printf("\n");
  free(bag);
}

int main() {
  srand((unsigned int)time(NULL));

  for (uint32_t i = 0; i < 20; i++) totally_random();
  printf("\n");
  for (uint32_t i = 0; i < 20; i++) test_bag();

  return 0;
}
