/*
 * Tetromino
 *
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "queue.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "tetromino-types.h"

static char name(uint8_t i) {
  const TetrominoType *type = &tetromino_types[i];
  return type->name;
}

static void test_push_peek_pop() {
  Queue *q = queue_new();

  queue_push_tail(q, 1);
  queue_push_tail(q, 2);
  queue_push_tail(q, 3);
  queue_push_tail(q, 4);
  queue_push_tail(q, 5);

  assert(queue_peek(q, 0) == 1);
  assert(queue_peek(q, 1) == 2);
  assert(queue_peek(q, 2) == 3);
  assert(queue_peek(q, 3) == 4);
  assert(queue_peek(q, 4) == 5);

  assert(queue_pop_head(q) == 1);
  assert(queue_pop_head(q) == 2);
  assert(queue_pop_head(q) == 3);
  assert(queue_pop_head(q) == 4);
  assert(queue_pop_head(q) == 5);

  free(q);
}

static void test_bag_queue() {
  Queue *q = queue_new();
  uint8_t *bag = random_bag_new();

  printf("bag:        ");
  for (uint8_t i = 0; i < 7; i++) {
    printf("%c ", name(bag[i]));
    queue_push_tail(q, bag[i]);
  }
  printf("\n");
  free(bag);

  printf("queue peek: ");
  for (uint8_t i = 0; i < 7; i++) printf("%c ", name(queue_peek(q, i)));
  printf("\n");

  printf("queue pop:  ");
  for (uint8_t i = 0; i < 7; i++) printf("%c ", name(queue_pop_head(q)));
  printf("\n");
}

static void test_next_queue() {
  Queue *q = queue_new_with_bag();
  for (uint16_t i = 0; i < 7 * 100; i++) {
    printf("%c ", name(queue_pop_next(q)));
    if ((i + 1) % 7 == 0) printf("\n");
  }
  free(q);
}

int main() {
  srand((unsigned int)time(NULL));

  test_push_peek_pop();
  test_bag_queue();
  test_next_queue();
  return 0;
}
