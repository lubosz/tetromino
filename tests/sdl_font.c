/*
 * Tetromino
 *
 * Copyright 2022 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <SDL.h>
#include <SDL_ttf.h>
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "config.h"

#ifndef USE_SDL1
SDL_Window *window;
#else
const SDL_VideoInfo *video;
#endif

SDL_Surface *surface;
TTF_Font *font;

static void _draw() {
  uint32_t color = 0xff7f00;
  SDL_Rect rect = {
      .x = 0, .y = 0, .h = DEFAULT_SCREEN_HEIGHT, .w = DEFAULT_SCREEN_WIDTH};
  SDL_FillRect(surface, &rect, color);

  SDL_Color white = {0xff, 0xff, 0xff, 0};

  // SDL_Surface *src = TTF_RenderUNICODE_Shaded(font, "Oh hai!", white, white);
  // SDL_Surface *src = TTF_RenderUNICODE_Blended(font, "Oh hai!", white);
  SDL_Surface *font_surface = TTF_RenderText_Solid(font, "Oh hai!", white);

  const SDL_PixelFormat *font_surface_fmt = font_surface->format;
  const SDL_PixelFormat *surface_fmt = font_surface->format;

  printf("font surface BPP %d, %dx%d\n", font_surface_fmt->BitsPerPixel,
         font_surface->w, font_surface->h);
  printf("     surface BPP %d, %dx%d\n", surface_fmt->BitsPerPixel, surface->w,
         surface->h);

  if (font_surface == NULL) {
    fprintf(stderr, "Unable to render text: %s\n", SDL_GetError());
    return;
  }

  SDL_Rect font_rect = {0, 0, 0, 0};

  rect.x = font_surface->w / 2;
  rect.y = font_surface->h / 2;

  SDL_BlitSurface(font_surface, NULL, surface, &font_rect);
  SDL_FreeSurface(font_surface);
}

int main() {
  /*
   * Initialize SDL
   */
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
    return EXIT_FAILURE;
  }

#ifdef USE_SDL1
  video = SDL_GetVideoInfo();

  if (video == NULL) {
    fprintf(stderr, "Unable to get video information: %s\n", SDL_GetError());
    return EXIT_FAILURE;
  }

  printf("Bits per pixel %d\n", video->vfmt->BitsPerPixel);
  surface = SDL_SetVideoMode(DEFAULT_SCREEN_WIDTH, DEFAULT_SCREEN_HEIGHT,
                             32,  // video->vfmt->BitsPerPixel,
                             SDL_HWSURFACE | SDL_DOUBLEBUF);

  if (surface == NULL) {
    fprintf(stderr, "Unable to set up video: %s\n", SDL_GetError());
    return EXIT_FAILURE;
  }
#else
  window = SDL_CreateWindow("Tetromino", SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED, DEFAULT_SCREEN_WIDTH,
                            DEFAULT_SCREEN_HEIGHT,
                            SDL_WINDOW_SHOWN | SDL_WINDOW_BORDERLESS);
  if (window == NULL) {
    fprintf(stderr, "Window could not be created! SDL_Error: %s\n",
            SDL_GetError());
    return EXIT_FAILURE;
  }
  surface = SDL_GetWindowSurface(window);
#endif

  // Init font

  if (TTF_Init() == -1) {
    fprintf(stderr, "Unable to initialize SDL_ttf: %s\n", TTF_GetError());
    return false;
  }

  int font_size = 128;
  font = TTF_OpenFont("PixelManiaConden.ttf", font_size);
  if (font == NULL) {
    fprintf(stderr, "Unable to load font file: %s\n", TTF_GetError());
    return false;
  }

  while (true) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) {
        return EXIT_SUCCESS;
      }
    }
    _draw();

#ifndef USE_SDL1
    SDL_UpdateWindowSurface(window);
#else
    SDL_Flip(surface);
#endif
  }

  return EXIT_SUCCESS;
}
