/*
 * Tetromino
 *
 * Copyright 2022 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <SDL.h>
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "config.h"

#ifndef USE_SDL1
SDL_Window *window;
#else
const SDL_VideoInfo *video;
#endif

SDL_Surface *surface;

static void _process_event(SDL_Event event) {
  switch (event.type) {
    case SDL_QUIT:
    case SDL_KEYUP:
      printf("Bye\n");
      exit(0);
      break;
    default:
      break;
  }
}

static void _poll_events() {
  SDL_Event event;
  while (SDL_PollEvent(&event)) _process_event(event);
}

static void _draw() {
  uint32_t color = 0xff7f00;
  SDL_Rect rect = {
      .x = 0, .y = 0, .h = DEFAULT_SCREEN_HEIGHT, .w = DEFAULT_SCREEN_WIDTH};
  SDL_FillRect(surface, &rect, color);
}

int main() {
  /*
   * Initialize SDL
   */
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
    return EXIT_FAILURE;
  }

#ifdef USE_SDL1
  video = SDL_GetVideoInfo();

  if (video == NULL) {
    fprintf(stderr, "Unable to get video information: %s\n", SDL_GetError());
    return EXIT_FAILURE;
  }

  printf("Bits per pixel %d\n", video->vfmt->BitsPerPixel);
  surface = SDL_SetVideoMode(DEFAULT_SCREEN_WIDTH, DEFAULT_SCREEN_HEIGHT,
                             32,  // video->vfmt->BitsPerPixel,
                             SDL_HWSURFACE | SDL_DOUBLEBUF);

  if (surface == NULL) {
    fprintf(stderr, "Unable to set up video: %s\n", SDL_GetError());
    return EXIT_FAILURE;
  }
#else
  window = SDL_CreateWindow("Tetromino", SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED, DEFAULT_SCREEN_WIDTH,
                            DEFAULT_SCREEN_HEIGHT,
                            SDL_WINDOW_SHOWN | SDL_WINDOW_BORDERLESS);
  if (window == NULL) {
    fprintf(stderr, "Window could not be created! SDL_Error: %s\n",
            SDL_GetError());
    return EXIT_FAILURE;
  }
  surface = SDL_GetWindowSurface(window);
#endif

  while (true) {
    _poll_events();
    if (SDL_MUSTLOCK(surface)) SDL_UnlockSurface(surface);
    _draw();
    if (SDL_MUSTLOCK(surface)) SDL_LockSurface(surface);

#ifndef USE_SDL1
    SDL_UpdateWindowSurface(window);
#else
    SDL_Flip(surface);
#endif
  }

  return EXIT_SUCCESS;
}
