/*
 * Tetromino
 *
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <assert.h>
#include <stdint.h>
#include <stdio.h>

#include "game.h"

int main() {
  GameState state;
  game_state_init(&state);

  for (uint32_t i = 0; i < 300; i++) {
    state.num_lines_cleared++;
    game_state_update_level(&state);
    printf("Level %.2d Tick %dms Lines %.3d\n", state.level,
           state.level_tick_ms, state.num_lines_cleared);
  }

  assert(state.level_tick_ms == MIN_TICK_MS);
  assert(state.level == 31);

  return 0;
}
