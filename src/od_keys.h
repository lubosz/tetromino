#pragma once

#include <SDL.h>

#define OD_UP SDLK_UP
#define OD_DOWN SDLK_DOWN
#define OD_RIGHT SDLK_RIGHT
#define OD_LEFT SDLK_LEFT

#define OD_START SDLK_RETURN
#define OD_SELECT SDLK_ESCAPE

#define OD_EAST SDLK_LCTRL
#define OD_WEST SDLK_LSHIFT
#define OD_NORTH SDLK_SPACE
#define OD_SOUTH SDLK_LALT

#define OD_RIGHT_ANALOG SDLK_KP_PERIOD
#define OD_LEFT_ANALOG SDLK_KP_DIVIDE

#define OD_R1 SDLK_BACKSPACE
#define OD_R2 SDLK_PAGEDOWN
#define OD_L1 SDLK_TAB
#define OD_L2 SDLK_PAGEUP

#define OD_POWER SDLK_HOME
#define OD_POWER_FIRST SDLK_POWER

#define OD_VOLUME_UP_OR_DOWN SDLK_UNKNOWN

#define ENUM_TO_STR(r) \
  case r:              \
    return #r

#ifdef USE_SDL1
typedef SDLKey SDL_Keycode;
#endif

const char* od_key_to_string(SDL_Keycode code) {
  switch (code) {
    ENUM_TO_STR(OD_UP);
    ENUM_TO_STR(OD_DOWN);
    ENUM_TO_STR(OD_RIGHT);
    ENUM_TO_STR(OD_LEFT);
    ENUM_TO_STR(OD_START);
    ENUM_TO_STR(OD_SELECT);
    ENUM_TO_STR(OD_EAST);
    ENUM_TO_STR(OD_WEST);
    ENUM_TO_STR(OD_NORTH);
    ENUM_TO_STR(OD_SOUTH);
    ENUM_TO_STR(OD_RIGHT_ANALOG);
    ENUM_TO_STR(OD_LEFT_ANALOG);
    ENUM_TO_STR(OD_R1);
    ENUM_TO_STR(OD_R2);
    ENUM_TO_STR(OD_L1);
    ENUM_TO_STR(OD_L2);
    ENUM_TO_STR(OD_POWER);
    ENUM_TO_STR(OD_POWER_FIRST);
    ENUM_TO_STR(OD_VOLUME_UP_OR_DOWN);
    default:
      printf("Unknown key %d.\n", code);
      return "UNKNOWN";
  }
}
