/*
 * Tetromino
 *
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <SDL.h>
#include <stdint.h>

typedef struct Board Board;

Board *board_new(void);
void board_update_from_extent(Board *self, uint16_t w, uint16_t h);
void board_draw(Board *self, SDL_Surface *surface);
uint32_t *board_get_state_ptr(Board *self);
void board_print(Board *self);
uint16_t board_get_unit_px(Board *self);

uint16_t board_get_max_x(Board *self);
uint16_t board_get_y(Board *self);
uint16_t board_get_x(Board *self);
uint32_t board_get_state(Board *self, uint8_t y, uint8_t x);
void board_set_state(Board *self, uint8_t y, uint8_t x, uint32_t color);
uint32_t board_update(Board *self);
void board_reset(Board *self);
uint16_t board_get_max_y(Board *self);
