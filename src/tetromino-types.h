/*
 * Tetromino
 *
 * Copyright 2005 Don E. Llopis <llopis.don@gmail.com>
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "color.h"
#include "tetromino-type.h"

/* ### */
/* #   */
// clang-format off
static TetrominoMask L_mask[4] = {{3, 2, {1, 1, 1,
                                          1, 0, 0}},

                                  {2, 3, {1, 1,
                                          0, 1,
                                          0, 1}},

                                  {3, 2, {0, 0, 1,
                                          1, 1, 1}},

                                  {2, 3, {1, 0,
                                          1, 0,
                                          1, 1}}};

/* ### */
/*   # */

static TetrominoMask J_mask[4] = {
  { 3, 2, {
      1, 1, 1,
      0, 0, 1,
      } },

  { 2, 3, {
      0, 1,
      0, 1,
      1, 1,
      } },

  { 3, 2, {
      1, 0, 0,
      1, 1, 1,
      } },

  { 2, 3, {
      1, 1,
      1, 0,
      1, 0,
      } }
};

/* ### */
/*  #  */

static TetrominoMask T_mask[4] = {
  { 3, 2, {
      1, 1, 1,
      0, 1, 0,
      } },

  { 2, 3, {
      0, 1,
      1, 1,
      0, 1,
      } },

  { 3, 2, {
      0, 1, 0,
      1, 1, 1,
      } },

  { 2, 3, {
      1, 0,
      1, 1,
      1, 0,
      } }
};

/*  ## */
/* ##  */

static TetrominoMask S_mask[2] = {
  { 3, 2, {
      0, 1, 1,
      1, 1, 0,
      } },

  { 2, 3, {
      1, 0,
      1, 1,
      0, 1,
      } }
};

/* ##  */
/*  ## */

static TetrominoMask Z_mask[2] = {
  { 3, 2, {
      1, 1, 0,
      0, 1, 1,
      } },

  { 2, 3, {
      0, 1,
      1, 1,
      1, 0,
      } }
};

/* #### */

static TetrominoMask I_mask[2] = {
  { 4, 1, {
      1, 1, 1, 1, 0, 0
      } },
  { 1, 4, {
      1,
      1,
      1,
      1,
      0, 0
      } }
};

/* ## */
/* ## */

static TetrominoMask O_mask[1] = {
  { 2, 2, {
      1, 1,
      1, 1,
      0, 0
      } }
};

#define NUM_TETROMINO_TYPES 7

static const TetrominoType tetromino_types[NUM_TETROMINO_TYPES] = {
  {0, 'L', 4, COLOR_ORANGE,  &L_mask[0]},
  {1, 'J', 4, COLOR_BLUE,    &J_mask[0]},
  {2, 'T', 4, COLOR_MAGENTA, &T_mask[0]},
  {3, 'S', 2, COLOR_GREEN,   &S_mask[0]},
  {4, 'Z', 2, COLOR_RED,     &Z_mask[0]},
  {5, 'I', 2, COLOR_CYAN,    &I_mask[0]},
  {6, 'O', 1, COLOR_YELLOW,  &O_mask[0]}
};
// clang-format on
