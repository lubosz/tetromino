/*
 * Tetromino
 *
 * Copyright 2005 Don E. Llopis <llopis.don@gmail.com>
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "game.h"

#include "color.h"
#include "config.h"
#include "tetromino-types.h"

typedef struct {
  bool left;
  bool right;
  bool down;
} PressState;


struct Game {
  bool is_hold_locked;

  // Tetromino is moved due to D-Pad keep being pressed
  PressState press_state;

  uint32_t last_moving_tick;

  GameState state;

  uint32_t last_tick;

  Board *board;

  const TetrominoType *hold;

  Tetromino *current;
  Queue *queue;
};

static bool _perform_down_move(Game *self);

void game_state_init(GameState *state) {
  state->is_over = false;
  state->is_paused = false;
  state->has_started = false;

  state->score = 0;
  state->num_lines_cleared = 0;

  state->level = 1;
  state->level_tick_ms =
      MIN_TICK_MS + LEVEL_TICK_DOWN_MS * (NUM_SPEED_LEVELS - 1);
}

GameState *game_get_state(Game *self) { return &self->state; }

static void _init(Game *self) {
  self->is_hold_locked = false;

  self->press_state.left = false;
  self->press_state.right = false;
  self->press_state.down = false;

  self->last_moving_tick = SDL_GetTicks();

  self->last_tick = SDL_GetTicks();

  self->current = NULL;
  self->hold = NULL;

  game_state_init(&self->state);

  self->board = board_new();

  self->queue = queue_new_with_bag();
}

#define MOVING_TIMEOUT 60
#define MOVING_FUTURE 300

bool _is_any_movement_pressed(Game *self)
{
  return self->press_state.left ||
         self->press_state.right ||
         self->press_state.down;
}

void game_set_press_state(Game *self, SDL_Keycode sym, bool pressed)
{

  // Started pressing some movement key
  if (pressed)
  {
    // Set last moving tick into the futre to have a small break
    self->last_moving_tick = SDL_GetTicks() + MOVING_FUTURE;
  }

  switch (sym)
  {
  case SDLK_LEFT:
    self->press_state.left = pressed;
    break;
  case SDLK_RIGHT:
    self->press_state.right = pressed;
    break;
  case SDLK_DOWN:
    self->press_state.down = pressed;
    break;
  default:
    break;
  }
}

/*
 * Reinit everything besides board
 * alternative would be to cache the window size
 * */
static void _reinit(Game *self) {
  free(self->queue);
  if (self->current) free(self->current);
  self->last_tick = SDL_GetTicks();
  self->current = NULL;
  game_state_init(&self->state);
  board_reset(self->board);
  self->queue = queue_new_with_bag();
}

Board *game_get_board(Game *self) { return self->board; }

Game *game_new() {
  Game *self = malloc(sizeof(Game));
  _init(self);
  return self;
}

static void _draw_hold(Game *self, SDL_Surface *surface) {
  uint16_t unit = board_get_unit_px(self->board);
  uint16_t x = board_get_x(self->board) - 4 * unit;
  uint16_t y = board_get_y(self->board) + 2 * unit;
  tetromino_mask_draw(&self->hold->mask[0], surface, y, x, unit,
                      self->hold->color);
}

static void _draw_next(Game *self, SDL_Surface *surface, uint8_t i) {
  uint8_t next = queue_peek(self->queue, i);
  const TetrominoType *type = &tetromino_types[next];

  uint16_t unit = board_get_unit_px(self->board);

  uint16_t x = board_get_max_x(self->board) + unit;
  uint16_t y = board_get_y(self->board) + 2 * unit;

  y += i * 3 * unit;

  uint16_t size = unit;

  if (i > 0) size = (uint16_t)(size * 0.75f);

  tetromino_mask_draw(&type->mask[0], surface, y, x, size, type->color);
}

void game_draw(Game *self, SDL_Surface *surface) {
  board_draw(self->board, surface);
  if (self->state.has_started) {
    tetromino_draw(self->current, surface);
    for (uint8_t i = 0; i < 6; i++) _draw_next(self, surface, i);
    if (self->hold) _draw_hold(self, surface);
  }
}

bool game_has_started(Game *self) { return self->state.has_started; }

void game_finalize(Game *self) {
  free(self->queue);
  free(self->board);
  if (self->current) free(self->current);
}

static void _spawn_tetromino_from_id(Game *self, uint8_t id) {
  if (self->current) free(self->current);

  self->current = tetromino_new(id, self->board);

  /* check for game over */
  if (!tetromino_is_position_valid(self->current)) self->state.is_over = true;

#ifdef DEBUG_GAME
  board_print(self->board);
#endif
}

static void _spawn_tetromino(Game *self) {
  _spawn_tetromino_from_id(self, queue_pop_next(self->queue));
}

void game_start(Game *self) {
  if (self->state.is_over) {
    _reinit(self);
    _spawn_tetromino(self);
  } else if (!self->state.has_started) {
    self->state.has_started = true;
    _spawn_tetromino(self);
  } else {
    self->state.is_paused = !self->state.is_paused;
  }
}

void game_toggle_hold(Game *self) {
  if (!self->current || self->is_hold_locked) return;

  const TetrominoType *new_hold = tetromino_get_type(self->current);

  if (self->hold)
    _spawn_tetromino_from_id(self, self->hold->id);
  else
    _spawn_tetromino(self);

  self->hold = new_hold;

  self->is_hold_locked = true;
}

void game_request_hard_drop(Game *self) {
  if (self->state.is_paused)
    return;

  while (!_perform_down_move(self));
}

/*
 * calculate game score based on NES scoring algorithm
 * see wikipedia.org entry for game for an explaination.
 */
static uint32_t _score(uint32_t level, uint32_t lines) {
  switch (lines) {
    case 1:
      return level * 40;
    case 2:
      return level * 100;
    case 3:
      return level * 300;
    case 4:
      return level * 1200;
    default:
      return 0;
  }
}

static void _state_update_score(GameState *state, uint32_t num_lines_cleared) {
  state->score += _score(state->level, num_lines_cleared);
  state->num_lines_cleared += num_lines_cleared;
}

void game_state_update_level(GameState *state) {
  uint32_t current_level = state->num_lines_cleared / NUM_LEVEL_UP_LINES + 1;
  if (current_level == state->level) return;

  /* Level up */
  state->level = current_level;
  state->level_tick_ms = MIN_TICK_MS;
  if (current_level < NUM_SPEED_LEVELS)
    state->level_tick_ms +=
        LEVEL_TICK_DOWN_MS * (NUM_SPEED_LEVELS - current_level);
}

static void _check_clear(Game *self) {
  uint32_t num_lines_cleared = board_update(self->board);
  _state_update_score(&self->state, num_lines_cleared);
  game_state_update_level(&self->state);

#ifdef DEBUG_GAME
  fprintf(stderr, "\n");
  fprintf(stderr, "cur level: %d\n", self->state.level);
  fprintf(stderr, "cur score: %d\n", self->state.score);
  fprintf(stderr, "num lines cleared: %d\n", self->state.num_lines_cleared);
  fprintf(stderr, "\n");
#endif
}

static void _on_collide(Game *self, int8_t prev_y) {
  /* move the tetromino back */
  tetromino_set_y(self->current, prev_y);
  tetromino_put(self->current);
  _check_clear(self);
  _spawn_tetromino(self);
  self->is_hold_locked = false;
}

/* move down one row */
static bool _perform_down_move(Game *self) {
  int8_t prev_y = tetromino_get_y(self->current);

  tetromino_move_down(self->current);

  if (tetromino_get_y(self->current) > tetromino_get_max_y(self->current))
  {
    _on_collide(self, prev_y); /* Tetromino reached board bottom */
    return true;
  }

  /* make sure we can move into this position */
  if (!tetromino_is_position_valid(self->current)) {
    if (tetromino_get_y(self->current) < 0) {
      self->state.is_over = true;
    } else {
      _on_collide(self, prev_y); /* Tetromino collided with other */
    }
    return true;
  }
  return false;
}

static void _wait_for_tick(Game *self, uint32_t timeout) {
  if ((SDL_GetTicks() - self->last_tick) < timeout) return;

  self->last_tick = SDL_GetTicks();
  _perform_down_move(self);
}

void game_iterate(Game *self) {
  if (!self->state.has_started || self->state.is_over || self->state.is_paused)
    return;
  _wait_for_tick(self, self->state.level_tick_ms);

  if (_is_any_movement_pressed(self)) {
    if ((int32_t)SDL_GetTicks() - (int32_t)self->last_moving_tick >
        MOVING_TIMEOUT) {
      if (self->press_state.left) {
        tetromino_move_left(self->current);
      } else if (self->press_state.right) {
        tetromino_move_right(self->current);
      }
      if (self->press_state.down) {
        tetromino_soft_drop(self->current);
      }
      self->last_moving_tick = SDL_GetTicks();
    }
  }
}

void game_move_left(Game *self) {
  if (!self->state.is_paused)
    tetromino_move_left(self->current);
}

void game_move_right(Game *self) {
  if (!self->state.is_paused)
    tetromino_move_right(self->current);
}

void game_rotate_cw(Game *self) {
  if (!self->state.is_paused)
    tetromino_rotate_cw(self->current);
}

void game_rotate_ccw(Game *self) {
  if (!self->state.is_paused)
    tetromino_rotate_ccw(self->current);
}

void game_soft_drop(Game *self) {
  if (!self->state.is_paused)
    tetromino_soft_drop(self->current);
}
