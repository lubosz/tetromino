/*
 * Tetromino
 *
 * Copyright 2005 Don E. Llopis <llopis.don@gmail.com>
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "app.h"

#include <SDL.h>
#include <SDL_ttf.h>
#include <time.h>

#include "color.h"
#include "game.h"
#include "od_keys.h"

#ifdef OPENDINGUX
  #define FONT_NAME "PixeloidSans.ttf"
#else
  #define FONT_NAME "res/PixeloidSans.ttf"
#endif

struct App {
#ifndef USE_SDL1
  SDL_Window *window;
#else
  const SDL_VideoInfo *video;
#endif
  SDL_Surface *surface;
  TTF_Font *font;

  bool is_running;

  int font_size;

  uint32_t w, h;
#ifndef USE_SDL1
  SDL_GameController *controller;
#endif
  bool fullscreen;

  Game *game;
};

App *app_new() {
  App *self = malloc(sizeof(App));
  return self;
}

bool app_is_running(App *self) { return self->is_running; }

bool app_init(App *self) {
  self->is_running = true;
  self->w = DEFAULT_SCREEN_WIDTH;
  self->h = DEFAULT_SCREEN_HEIGHT;
#ifndef USE_SDL1
  self->controller = NULL;
#endif
  self->fullscreen = false;

  printf("Initializing tetromino with %dx%d\n", self->w, self->h);

  srand((unsigned int)time(NULL));

  /*
   * Initialize Font Engine and load the font
   */
  if (TTF_Init() == -1) {
    fprintf(stderr, "Unable to initialize SDL_ttf: %s\n", TTF_GetError());
    return false;
  }

  /*
   * Initialize SDL Video and Audio
   */
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO
#ifndef USE_SDL1
               | SDL_INIT_GAMECONTROLLER
#endif
               ) < 0) {
    fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
    return false;
  }

#ifdef USE_SDL1
  printf("Initializing SDL1\n");
  self->video = SDL_GetVideoInfo();

  printf("Detected resolution: %dx%d @ %dbpp\n", self->video->current_w,
         self->video->current_h, self->video->vfmt->BitsPerPixel);

  if (self->video == NULL) {
    fprintf(stderr, "Unable to get video information: %s\n", SDL_GetError());
    return false;
  }

#ifdef OPENDINGUX
  const int supported_resolutions[4][2] = {
      {640, 480},
      {320, 240},
      {240, 160},
      {self->video->current_w, self->video->current_h}
  };

  printf("Supported modes:\n");
  for (uint32_t i = 0; i < 4; i++) {
    int test_w = supported_resolutions[i][0];
    int test_h = supported_resolutions[i][1];
    int supported_bpp = SDL_VideoModeOK(test_w, test_h, 32, SDL_ANYFORMAT);
    printf("%dx%d @ %dbpp\n", test_w, test_h, supported_bpp);
    if (supported_bpp == 32) {
      self->w = test_w;
      self->h = test_h;
      printf("Chosing mode %dx%d @ 32bpp\n", self->w, self->h);
      break;
    }
  }

  if (self->w == 0 || self->h == 0) {
    printf("Could not find suitable mode.\n");
  }

#else
  self->w = DEFAULT_SCREEN_WIDTH;
  self->h = DEFAULT_SCREEN_HEIGHT;
#endif

  self->surface = SDL_SetVideoMode((int)self->w, (int)self->h,
                                   32,
                                   SDL_HWSURFACE | SDL_DOUBLEBUF);

  if (self->surface == NULL) {
    fprintf(stderr, "Unable to set up video: %s\n", SDL_GetError());
    return false;
  }
#else
  printf("Initializing SDL2\n");
  self->window = SDL_CreateWindow(
      "Tetromino", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
      (int)self->w, (int)self->h, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
  if (self->window == NULL) {
    fprintf(stderr, "Window could not be created! SDL_Error: %s\n",
            SDL_GetError());
    return false;
  }
  self->surface = SDL_GetWindowSurface(self->window);
  SDL_SetWindowIcon(self->window, SDL_LoadBMP("sdlblocks.bmp"));
#endif

  self->game = game_new();

  Board *board = game_get_board(self->game);
  board_update_from_extent(board, self->w, self->h);

  self->font_size = board_get_unit_px(board);

  if (self->font_size == 10) {
    self->font_size = 9;
  } else if (self->font_size == 21) {
    self->font_size = 18;
  }

  self->font = TTF_OpenFont(FONT_NAME, self->font_size);
  if (self->font == NULL) {
    fprintf(stderr, "Unable to load font file: %s %s\n", FONT_NAME,
            TTF_GetError());
    return false;
  }

  printf("Using font %s @ %d points.\n", FONT_NAME, self->font_size);

  return true;
}

void app_finalize(App *self) {
  game_finalize(self->game);
  free(self->game);
#ifndef USE_SDL1
  if (self->controller) SDL_GameControllerClose(self->controller);
#endif
  SDL_FreeSurface(self->surface);
  TTF_CloseFont(self->font);
  TTF_Quit();
  SDL_Quit();
  printf("Play Tetromino!\n");
}

static void action_start(App *self) { game_start(self->game); }

static void action_quit(App *self) { self->is_running = false; }

#ifndef USE_SDL1
static void _resize(App *self, uint32_t w, uint32_t h) {
  self->w = w;
  self->h = h;
  self->surface = SDL_GetWindowSurface(self->window);
  Board *board = game_get_board(self->game);
  board_update_from_extent(board, (uint16_t)self->w, (uint16_t)self->h);

  self->font_size = board_get_unit_px(board);

  TTF_CloseFont(self->font);
  self->font = TTF_OpenFont(FONT_NAME, self->font_size);
  if (self->font == NULL) {
    fprintf(stderr, "Unable to load font file: %s %s\n", FONT_NAME,
            TTF_GetError());
  }
}

static void _process_window_event(App *self, SDL_WindowEvent event) {
  switch (event.event) {
    case SDL_WINDOWEVENT_SIZE_CHANGED:
      _resize(self, (uint32_t)event.data1, (uint32_t)event.data2);
      break;
    default:
      break;
  }
}

#define SDL_CONTROLLER_BUTTON_SOUTH SDL_CONTROLLER_BUTTON_A
#define SDL_CONTROLLER_BUTTON_EAST SDL_CONTROLLER_BUTTON_B
#define SDL_CONTROLLER_BUTTON_WEST SDL_CONTROLLER_BUTTON_X
#define SDL_CONTROLLER_BUTTON_NORTH SDL_CONTROLLER_BUTTON_Y

static void _process_controller_event(App *self, SDL_ControllerButtonEvent e) {
  switch (e.button) {
    case SDL_CONTROLLER_BUTTON_SOUTH:
      game_rotate_cw(self->game);
      break;
    case SDL_CONTROLLER_BUTTON_EAST:
      game_rotate_ccw(self->game);
      break;
    case SDL_CONTROLLER_BUTTON_WEST:
      break;
    case SDL_CONTROLLER_BUTTON_NORTH:
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_UP:
      game_request_hard_drop(self->game);
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
      game_soft_drop(self->game);
      game_set_press_state(self->game, SDLK_DOWN, true);
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
      game_move_left(self->game);
      game_set_press_state(self->game, SDLK_LEFT, true);
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
      game_move_right(self->game);
      game_set_press_state(self->game, SDLK_RIGHT, true);
      break;
    case SDL_CONTROLLER_BUTTON_START:
      action_start(self);
      break;
    case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
      game_toggle_hold(self->game);
      break;
    default:
      break;
  }
}

static void _process_controller_up_event(App *self,
                                         SDL_ControllerButtonEvent e) {
  switch (e.button) {
    case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
      game_set_press_state(self->game, SDLK_LEFT, false);
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
      game_set_press_state(self->game, SDLK_RIGHT, false);
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
      game_set_press_state(self->game, SDLK_DOWN, false);
      break;
    default:
      break;
  }
}
#endif

static void _process_game_key_event(Game *game, SDL_Keycode sym) {
  if (!game_has_started(game)) return;

  switch (sym) {
#ifdef OPENDINGUX
    case OD_LEFT:
      game_move_left(game);
      game_set_press_state(game, SDLK_LEFT, true);
      break;
    case OD_RIGHT:
      game_move_right(game);
      game_set_press_state(game, SDLK_RIGHT, true);
      break;
    case OD_SOUTH:
      game_rotate_cw(game);
      break;
    case OD_EAST:
      game_rotate_ccw(game);
      break;
    case OD_DOWN:
      game_soft_drop(game);
      game_set_press_state(game, SDLK_DOWN, true);
      break;
    case OD_UP:
      game_request_hard_drop(game);
      break;
    case OD_R1:
      game_toggle_hold(game);
#else
    case SDLK_LEFT:
      game_move_left(game);
      game_set_press_state(game, SDLK_LEFT, true);
      break;
    case SDLK_RIGHT:
      game_move_right(game);
      game_set_press_state(game, SDLK_RIGHT, true);
      break;
    case SDLK_UP:
    case SDLK_x:
      game_rotate_cw(game);
      break;
    case SDLK_z:
    case SDLK_LCTRL:
      game_rotate_ccw(game);
      break;
    case SDLK_DOWN:
      game_soft_drop(game);
      game_set_press_state(game, SDLK_DOWN, true);
      break;
    case SDLK_SPACE:
      game_request_hard_drop(game);
      break;
    case SDLK_LSHIFT:
    case SDLK_c:
      game_toggle_hold(game);
#endif
    default:
      break;
  }
}

static void _process_game_key_up_event(Game *game, SDL_Keycode sym) {
  if (!game_has_started(game)) return;

  switch (sym) {
    case SDLK_LEFT:
      game_set_press_state(game, SDLK_LEFT, false);
      break;
    case SDLK_RIGHT:
      game_set_press_state(game, SDLK_RIGHT, false);
      break;
    case SDLK_DOWN:
      game_set_press_state(game, SDLK_DOWN, false);
      break;
    default:
      break;
  }
}

static void _process_key_event(App *self, SDL_Keycode sym) {
  printf("Key Event %d (%s)\n", sym, od_key_to_string(sym));

  _process_game_key_event(self->game, sym);

  switch (sym) {
    case SDLK_RETURN:
      action_start(self);
      break;
    case SDLK_ESCAPE:
      action_quit(self);
      break;
#ifndef USE_SDL1
    case SDLK_f:
      self->fullscreen = !self->fullscreen;
      SDL_SetWindowFullscreen(
          self->window, self->fullscreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0);
      break;
#endif
    default:
      break;
  }
}

static void _process_key_up_event(App *self, SDL_Keycode sym) {
  _process_game_key_up_event(self->game, sym);
}

static void _process_event(App *self, SDL_Event event) {
  switch (event.type) {
    case SDL_QUIT:
      action_quit(self);
      break;
    case SDL_KEYDOWN:
#ifndef USE_SDL1
      // Ignore keyboard repeat events on SDL2
      if (event.key.repeat == 1) {
        return;
      }
#endif
      _process_key_event(self, event.key.keysym.sym);
      break;
    case SDL_KEYUP:
      _process_key_up_event(self, event.key.keysym.sym);
      break;
#ifndef USE_SDL1
    case SDL_WINDOWEVENT:
      _process_window_event(self, event.window);
      break;
    case SDL_CONTROLLERBUTTONDOWN:
      _process_controller_event(self, event.cbutton);
      break;
    case SDL_CONTROLLERBUTTONUP:
      _process_controller_up_event(self, event.cbutton);
      break;
    case SDL_CONTROLLERDEVICEADDED:
      if (!self->controller) {
        self->controller = SDL_GameControllerOpen(event.cdevice.which);
        if (self->controller)
          printf("Found controller %d: %s\n", event.cdevice.which,
                 SDL_GameControllerName(self->controller));
        else
          fprintf(stderr, "Could not open controller %i: %s\n",
                  event.cdevice.which, SDL_GetError());
      }
      break;
    case SDL_CONTROLLERDEVICEREMOVED:
      if (self->controller) {
        printf("Detaching controller %d: %s\n", event.cdevice.which,
               SDL_GameControllerName(self->controller));
        SDL_GameControllerClose(self->controller);
        self->controller = NULL;
      }
      break;
#endif
    default:
      break;
  }
}

static void _poll_events(App *self) {
  SDL_Event event;
  while (SDL_PollEvent(&event)) _process_event(self, event);
}

typedef enum { TEXT_ALIGN_LEFT, TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER } TextAlign;

static void _draw_text_line(App *self, int x, int y, char *text,
                            SDL_Color color, TextAlign align) {
  SDL_Surface *src = TTF_RenderText_Solid(self->font, text, color);
  if (src == NULL) return;

  SDL_Rect rect = {.w = src->w, .h = src->h};

  switch (align) {
    case TEXT_ALIGN_LEFT:
      rect.x = x;
      rect.y = y - src->h;
      break;
    case TEXT_ALIGN_RIGHT:
      rect.x = x - src->w;
      rect.y = y - src->h;
      break;
    case TEXT_ALIGN_CENTER:
      rect.x = x - src->w / 2;
      rect.y = y - src->h / 2;
      break;
  }

  SDL_BlitSurface(src, NULL, self->surface, &rect);
  SDL_FreeSurface(src);
}

static void _draw_text(App *self) {
  SDL_Color white = {0xff, 0xff, 0xff, 0};

  char text[256];

  Board *board = game_get_board(self->game);

  uint16_t x = board_get_max_x(board) + board_get_unit_px(board);
  uint16_t y = board_get_y(board) + board_get_unit_px(board);
  _draw_text_line(self, x, y, "Next", white, TEXT_ALIGN_LEFT);

  x = board_get_x(board) - board_get_unit_px(board);

  _draw_text_line(self, x, y, "Hold", white, TEXT_ALIGN_RIGHT);
  y += self->font_size * 8;

  y = board_get_max_y(board);

  GameState *state = game_get_state(self->game);
  sprintf(&text[0], "level: %d", state->level);
  _draw_text_line(self, x, y, &text[0], white, TEXT_ALIGN_RIGHT);
  y -= self->font_size;
  sprintf(&text[0], "lines: %d", state->num_lines_cleared);
  _draw_text_line(self, x, y, &text[0], white, TEXT_ALIGN_RIGHT);
  y -= self->font_size;
  sprintf(&text[0], "score: %d", state->score);
  _draw_text_line(self, x, y, &text[0], white, TEXT_ALIGN_RIGHT);
  y -= self->font_size * 3;

  x = (board_get_x(board) + board_get_max_x(board)) / 2;
  y = (board_get_y(board) + board_get_max_y(board)) / 2;

  if (state->is_paused)
    _draw_text_line(self, x, y, "PAUSE", white, TEXT_ALIGN_CENTER);
  else if (!state->has_started)
    _draw_text_line(self, x, y, "Tetromino", white, TEXT_ALIGN_CENTER);
  else if (state->is_over)
    _draw_text_line(self, x, y, "GAME OVER", white, TEXT_ALIGN_CENTER);
}

static void _draw(App *self) {
  if (SDL_MUSTLOCK(self->surface)) SDL_UnlockSurface(self->surface);

  /* Draw the background */
  SDL_FillRect(self->surface, NULL, COLOR_BLACK);

  game_draw(self->game, self->surface);

  _draw_text(self);

  if (SDL_MUSTLOCK(self->surface)) SDL_LockSurface(self->surface);

#ifndef USE_SDL1
  SDL_UpdateWindowSurface(self->window);
#else
  SDL_Flip(self->surface);
#endif
}

void app_iterate(App *self) {
  _poll_events(self);
  game_iterate(self->game);
  _draw(self);
}
