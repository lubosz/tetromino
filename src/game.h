/*
 * Tetromino
 *
 * Copyright 2005 Don E. Llopis <llopis.don@gmail.com>
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <SDL.h>
#include <stdbool.h>
#include <stdint.h>

#include "board.h"
#include "config.h"
#include "queue.h"
#include "tetromino.h"

/* The number of levels where we increase speed */
#define NUM_SPEED_LEVELS 20
/* The drop speed at max level */
#define MIN_TICK_MS 100
/* The amount of ms the tick timeout decreases per level */
#define LEVEL_TICK_DOWN_MS 20
/* The required amount of cleared lines for leveling up */
#define NUM_LEVEL_UP_LINES 10

typedef struct {
  bool is_over;
  bool is_paused;
  bool has_started;

  uint32_t level;
  uint32_t level_tick_ms;

  uint32_t score;
  uint32_t num_lines_cleared;
} GameState;

typedef struct Game Game;

Game *game_new(void);
void game_finalize(Game *self);

void game_iterate(Game *self);
void game_start(Game *self);
void game_request_hard_drop(Game *self);
Board *game_get_board(Game *self);

void game_move_left(Game *self);
void game_move_right(Game *self);
void game_rotate_cw(Game *self);
void game_rotate_ccw(Game *self);
void game_soft_drop(Game *self);
void game_draw(Game *self, SDL_Surface *surface);
bool game_has_started(Game *self);
GameState *game_get_state(Game *self);
void game_toggle_hold(Game *self);

#ifdef USE_SDL1
typedef SDLKey SDL_Keycode;
#endif

void game_set_press_state(Game *self, SDL_Keycode sym, bool pressed);

void game_state_update_level(GameState *state);
void game_state_init(GameState *state);
