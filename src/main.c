/*
 * Tetromino
 *
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdio.h>
#include <stdlib.h>

#include "app.h"

int main() {
  App *app = app_new();

  if (!app_init(app)) {
    fprintf(stderr, "App init failed.\n");
    return -1;
  }

  while (app_is_running(app)) app_iterate(app);

  app_finalize(app);

  free(app);

  return 0;
}
